import logging
import pandas as pd
from telegram import Update, ForceReply
from telegram.ext import Updater, CommandHandler, MessageHandler, Filters, CallbackContext
import test
from GoogleNews import GoogleNews
from newspaper import Article
import pandas as pd
from datetime import date
from time import sleep

import dropbox


dropbox_access_token = ''

def upload_file(file_from, file_to):
        """upload a file to Dropbox using API v2
        """
        dbx = dropbox.Dropbox(dropbox_access_token)

        with open(file_from, 'rb') as f:
            dbx.files_upload(f.read(), file_to)
            
def returnlink(file_to):
        dbx = dropbox.Dropbox(dropbox_access_token)
        location = file_to
        link = dbx.sharing_create_shared_link_with_settings(location)
        #print(link.url)
        return(link.url)   

token = ''

# Enable logging
logging.basicConfig(
    format='%(asctime)s - %(name)s - %(levelname)s - %(message)s', level=logging.INFO
)

logger = logging.getLogger(__name__)


# Define a few command handlers. These usually take the two arguments update and
# context.
def start(update: Update, _: CallbackContext) -> None:
    """Send a message when the command /start is issued."""
    user = update.effective_user
    update.message.reply_markdown_v2(
        fr'Hi {user.mention_markdown_v2()}\!',
        reply_markup=ForceReply(selective=True),
    )


def help_command(update: Update, _: CallbackContext) -> None:
    """Send a message when the command /help is issued."""
    update.message.reply_text('Help!')

def reply(returnmessage, update: Update, _: CallbackContext) -> None:
    """Send a message when the command /help is issued."""
    update.message.reply_text(returnmessage)
    
def get_the_news(update: Update, _: CallbackContext) -> None:
    input = (update.message.text).lower()   
    input = input[14:]
    print(input)
    search = input
    start='01/01/2021'
    end='03/01/2021'
    googlenews=GoogleNews(start=start,end=end)
    googlenews.search(search)
    result=googlenews.result()
    df=pd.DataFrame(result)    
        
    for i in range(1,20):
        sleep(1)
        googlenews.getpage(i)
        result=googlenews.result()
        df1 = pd.DataFrame(result)

        if len(df1.index)>2:
            df=pd.concat([df, df1])
        else:
            break
    
    name = search.replace(' ', '_')
    
    today = date.today()
    df['extract_date'] = today.strftime("%d-%m-%Y")
    
    output = 'output/google_' + name + '_' + today.strftime("%d-%m-%Y") +'.csv'    
    
    df.to_csv(output)
    
    print(output + ' saved')


    """Send a message when the command /help is issued."""
    #update.message.reply_text((output + 'saved')) 
    
    file_to = f'/Get_The_News/{input}.csv'
    
    upload_file(output, file_to)  
    link = returnlink(file_to)
    print(link)
    
    update.message.reply_text((f'your file is viewable at: {link}')) 
    
    
def find_relative_input(update: Update, _: CallbackContext):    
    input = (update.message.text).lower()   
    print(input)
    input = input[14:]
    print(input)
    df2 = pd.read_csv('check_data.csv')
   
    search_name = input.split(",")     
    print(len(search_name))

    similarity1 = []

    if 'id' in df2.columns:
        pass
    else:
        df2.insert(0,'id',range(0,0+len(df2)))
        
    df2['Score'] = ''
    df2['Matched_Word'] = ''  

    for j in search_name:        
        for i in df2['id']:
            rowparsedname = df2.at[i,'Parsed_Name']
            rowname = df2.at[i,'Name']
            if j in rowname:                
                similarity1.append(rowparsedname) 
                
    print(len(similarity1))
                
    if len(similarity1) <= 0:
        update.message.reply_text("No results")   
    else:
        update.message.reply_text(similarity1)    

      
    
def main() -> None:
    
    updater = Updater(
    token=token,
    use_context=True,    
)
    
    """Start the bot."""
    # Create the Updater and pass it your bot's token.
    

    # Get the dispatcher to register handlers
    dispatcher = updater.dispatcher

    # on different commands - answer in Telegram
    dispatcher.add_handler(CommandHandler("start", start))
    dispatcher.add_handler(CommandHandler("help", help_command))
    #dispatcher.add_handler(CommandHandler("echo", echo))
    dispatcher.add_handler(CommandHandler("find_relative", find_relative_input))
    dispatcher.add_handler(CommandHandler("get_the_news", get_the_news))

    # on non command i.e message - echo the message on Telegram
    #dispatcher.add_handler(MessageHandler(Filters.text, find_relative_input))

    # Start the Bot
    updater.start_polling()

    # Run the bot until you press Ctrl-C or the process receives SIGINT,
    # SIGTERM or SIGABRT. This should be used most of the time, since
    # start_polling() is non-blocking and will stop the bot gracefully.
    updater.idle()    

if __name__ == '__main__':
    main()
